#![feature(test)]
#![feature(let_chains)]
use std::{
    array,
    fmt::Display,
    io::{self, stdin, ErrorKind},
    vec,
};

extern crate test;

#[derive(Debug)]
enum SolveError {
    Contradiction,
    TooManyIterations,
}

impl Display for SolveError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SolveError::Contradiction => todo!(),
            SolveError::TooManyIterations => todo!(),
        }
    }
}

#[derive(Default, Clone, Debug)]
struct Board([[Vec<u8>; 9]; 9]);

fn reduce_to_options(elements: impl Iterator<Item = Vec<u8>>) -> Vec<u8> {
    elements
        .map(|mut row| {
            row.sort();
            row.dedup();
            row
        })
        .fold((1..=9).collect(), |mut seen, current| {
            if current.len() == 1 {
                if let Some(pos) = seen.iter().position(|&s| s == current[0]) {
                    seen.swap_remove(pos);
                }
            }

            seen
        })
}

impl Board {
    fn new() -> Self {
        Board(array::from_fn(|_| array::from_fn(|_| (1..=9).collect())))
    }

    fn from_csv(csv: String) -> Self {
        Board::from(
            csv.split([',', '\n'])
                .map(|el| el.trim().parse::<u8>().ok())
                .collect::<Vec<Option<u8>>>(),
        )
    }

    fn solve_step_mut(&mut self) -> Result<&Board, SolveError> {
        let frozen = self.clone();
        for (row_idx, row) in self.0.iter_mut().enumerate() {
            let row_options: Vec<u8> = frozen.get_row_options(row_idx);
            for (col_idx, ele) in row
                .iter_mut()
                .enumerate()
                .filter(|(_, elem)| elem.len() != 1)
            {
                let column_options: Vec<u8> = frozen.get_column_options(col_idx);
                let cell_options: Vec<u8> = frozen.get_surrounding_options(row_idx, col_idx);

                let options: Vec<u8> = (1..=9)
                    .filter(|x| {
                        row_options.contains(x)
                            && column_options.contains(x)
                            && cell_options.contains(x)
                    })
                    .collect();

                *ele = options;
            }
        }

        Ok(self)
    }

    fn solve(&mut self) -> Result<&Board, SolveError> {
        let mut iterations = 0;
        loop {
            self.solve_step_mut()?;
            iterations += 1;
            if self.is_solved() {
                break;
            }
            if iterations == u8::MAX {
                return Err(SolveError::TooManyIterations);
            }
        }

        Ok(self)
    }

    fn is_solved(&self) -> bool {
        self.0
            .iter()
            .all(|row| row.iter().all(|entry| entry.len() == 1))
    }

    fn get_column_options(&self, col_idx: usize) -> Vec<u8> {
        // TODO: The remaining possibilities (of unsolved cells) probably also reduce the
        // possible cell entry. Count them in.
        reduce_to_options(self.0.iter().map(|row| row[col_idx].clone()))
    }

    fn get_row_options(&self, row_idx: usize) -> Vec<u8> {
        // TODO: The remaining possibilities (of unsolved cells) probably also reduce the
        // possible cell entry. Count them in.
        reduce_to_options(self.0[row_idx].iter().cloned())
    }

    fn get_surrounding_options(&self, row_idx: usize, col_idx: usize) -> Vec<u8> {
        // idx - (idx % 3) 'rounds' idx down to and idx + (3 - (idx % 3)) 'rounds' idx up to
        // the closest multiple of 3
        let cell_boundaries = |idx| idx - (idx % 3)..idx + (3 - (idx % 3));

        reduce_to_options(
            self.0[cell_boundaries(row_idx)]
                .iter()
                .flat_map(|row| row[cell_boundaries(col_idx)].to_vec()),
        )
    }

    fn init_random() -> Self {
        let mut empty_board = Board::new();
        let mut iterations = 0;

        if let Some(cell) = empty_board.get_mut(0, 0) {
            *cell = vec![1];
        }

        loop {
            if iterations == u8::MAX {
                break;
            }

            let not_yet_filled = empty_board.0.iter().enumerate().flat_map(|(row_idx, row)| {
                row.iter()
                    .enumerate()
                    .filter(|(_, column)| column.len() > 1)
                    .map(move |(col_idx, _)| (row_idx, col_idx))
            });

            if let Some((row, column)) = fastrand::choice(not_yet_filled.collect::<Vec<_>>()) {
                let options: Vec<u8> = (1..=9)
                    .filter(|x| {
                        empty_board.get_row_options(row).contains(x)
                            && empty_board.get_column_options(column).contains(x)
                            && empty_board.get_surrounding_options(row, column).contains(x)
                    })
                    .collect();
                println!("Possibilities at {:?}: {:?}", (row, column), options);

                if let Some(choice) = fastrand::choice(options)
                    && let Some(cell) = empty_board.get_mut(row as u8, column as u8)
                {
                    println!("{choice}");
                    *cell = vec![choice];
                    println!("{empty_board}")
                };
            };
            iterations += 1;
            stdin().read_line(&mut String::default());
        }

        empty_board
    }

    /// Index the underlying 2D Vec mutably if the indices are in range.
    /// Mimicks [`SliceIndex::get_mut`]
    fn get_mut(&mut self, row_idx: u8, col_idx: u8) -> Option<&mut Vec<u8>> {
        self.0
            .get_mut(row_idx as usize)
            .and_then(|row| row.get_mut(col_idx as usize))
    }
}

impl From<Vec<Option<u8>>> for Board {
    fn from(value: Vec<Option<u8>>) -> Self {
        Board(array::from_fn(|row_idx| {
            array::from_fn(|col_idx| match value.get(row_idx * 9 + col_idx) {
                Some(Some(v)) => vec![*v],
                _ => (1..=9).collect(),
            })
        }))
    }
}

impl From<String> for Board {
    fn from(value: String) -> Self {
        Self::from_csv(value)
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        #![allow(unused_must_use)]
        writeln!(f, "┌───{}┐", "┬───".repeat(8));
        for (row_idx, row) in self.0.iter().enumerate() {
            row.iter().for_each(|cell| {
                if cell.is_empty() {
                    write!(f, "| X ")
                } else if cell.len() == 1 {
                    write!(f, "| {} ", cell[0])
                } else {
                    write!(f, "|   ")
                };
            });

            if row_idx + 1 == self.0.len() {
                writeln!(f, "|\n└───{}┘", "┴───".repeat(8));
            } else {
                writeln!(f, "|\n├───{}┤", "┼───".repeat(8));
            }
        }

        Ok(())
    }
}

impl Iterator for Board {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.iter().flat_map(|row| row.iter()).next().cloned()
    }
}

fn main() -> io::Result<()> {
    let inp = &mut String::new();

    // stdin().read_line(inp)?;

    let mut board = Board::from(inp.to_owned());
    let mut _board = Board::init_random();
    print!("Initial:\n{_board}");

    _board
        .solve()
        .map_err(|err| io::Error::new(ErrorKind::Other, err.to_string()))?;
    println!("{_board}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_surrounding_square() {
        let board = Board::from_csv(
           "6, ,4,1, , , ,3,8, 9,2, ,8, ,5, ,1,6, ,3,8, , ,6,5,9,7, , ,1, ,6,4,9, ,2, 4,9,2,5,8,3,7,6,1, 7,6, ,9, ,2,8,4,3, 8,4,6,3,7, ,1,2,5, ,7, ,4,2, ,6,8, , , ,9,6,5,8,3, ,4"
             .to_owned());

        let mut surrounding = board.get_surrounding_options(0, 0);
        surrounding.sort();
        assert_eq!(surrounding, [1, 5, 7])
    }

    #[bench]
    fn solving_time(b: &mut test::Bencher) {
        b.iter(|| {let mut board = Board::from_csv(
                "6, ,4,1, , , ,3,8, 9,2, ,8, ,5, ,1,6, ,3,8, , ,6,5,9,7, , ,1, ,6,4,9, ,2, 4,9,2,5,8,3,7,6,1, 7,6, ,9, ,2,8,4,3, 8,4,6,3,7, ,1,2,5, ,7, ,4,2, ,6,8, , , ,9,6,5,8,3, ,4"
                .to_owned());
            board.solve();})
    }
}
